#-------------------------------------------------
#
# Project created by QtCreator 2014-05-15T19:17:06
#
#-------------------------------------------------

QT       += core sql gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TRANSLATIONS += rsc\translations\q_contatos_br.ts \
                rsc\translations\q_contatos_en.ts \
                rsc\translations\q_contatos_es.ts

TARGET = QContatos
TEMPLATE = app

# install
target.path = dlls
INSTALLS += target

SOURCES += main.cpp\
        maincontatos.cpp \
    connection.cpp \
    listparaedicao.cpp \
    widtradutor.cpp \
    widcontatos.cpp

HEADERS  += maincontatos.h \
    connection.h \
    listparaedicao.h \
    widtradutor.h \
    widcontatos.h

FORMS    += maincontatos.ui \
    listparaedicao.ui \
    widtradutor.ui \
    widcontatos.ui

CONFIG += mobility
MOBILITY = 

RESOURCES += \
    rsc/resource.qrc

