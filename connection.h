#ifndef CONNECTION_H
#define CONNECTION_H

#include <QObject>
#include <QSqlDatabase>
#include <QSqlQueryModel>
#include <QStandardItemModel>

class connection : public QObject
{
    Q_OBJECT
public:


    explicit connection(QObject *parent = 0);
    virtual ~connection();

    bool createTable(QString a_tableName, QList<QPair<QString, QString> > a_listCamposTipo);
    bool insertTable(QString a_tableName, QStringList a_listValues);
    bool deleteRow(QString a_tableName);
    QStandardItemModel *selectAll(QStandardItemModel * a_model = NULL);

//Static functions
    static bool createConnection();
    static QSqlDatabase* instanceDB();
    static void release();

private:
    static QSqlDatabase * m_db;

signals:
    void signal_criarLista();

public slots:
    void slot_deleteRow(QString a_nome, QString a_contato);
    bool slot_update(QString a_nomeNovo, QString a_contatoNovo, QString a_nomeAntigo, QString a_contatoAntigo);

};

#endif // CONNECTION_H
