#include "maincontatos.h"
#include "ui_maincontatos.h"
#include "connection.h"
#include "listparaedicao.h"
#include "widcontatos.h"
#include "widtradutor.h"

#include <QMessageBox>
#include <QSqlQueryModel>
#include <QDesktopWidget>
#include <QDebug>

MainContatos::MainContatos(QWidget *parent) :
    QMainWindow(parent),
    m_stackedWidget(0),
    m_widContatos(0),
    m_widTradutor(0),
    m_listaDeEdicao(0),
    ui(new Ui::MainContatos)
{
    ui->setupUi(this);

    style();

    setOrientation();

    setWindowTitle(tr("QContatos"));

    //Criação da tabela contatos
    QList<QPair<QString, QString> > values;

    values.append(QPair<QString, QString>("nome", "TEXT"));
    values.append(QPair<QString, QString>("contato", "TEXT"));

    m_connectionDB = new connection(this);
    m_connectionDB->createTable("contato_db", values);

    m_listaDeEdicao = new ListParaedicao(this);
    m_listaDeEdicao->hide();

    //Instanciando o model para as listagens
    m_model = new QStandardItemModel(this);

    //Instanciando elementos gráficos
    initUi();

    //A geração de connects só poderá ocorrer após a instanciação dos elementos
    gerarConnects();

}

MainContatos::~MainContatos()
{
    delete ui;
}

void MainContatos::setSizeModalWidget(QWidget *a_widget, int a_parentHeight, int a_parentWidth)
{
    //seto o tamanho da widget em relação do pai
    //defino o valor da proporção em relação do pai
    a_widget->setMinimumHeight(a_parentHeight * 0.80);
    a_widget->setMinimumWidth(a_parentWidth * 0.85);

}

void MainContatos::initUi()
{
    //Iniciando as UIs
    iniciarWidContatos();
    iniciarWidListaDeContados();
    iniciarWidTradutor();

    //Inicar o Stacked
    m_stackedWidget = new QStackedWidget(this);

    m_stackedWidget->insertWidget(WID_CONTATOS, m_widContatos);
    m_stackedWidget->insertWidget(WID_EDICAO, m_listaDeEdicao);

    m_stackedWidget->widget(WID_CONTATOS)->setVisible(true);
    m_stackedWidget->widget(WID_EDICAO)->setVisible(true);

    m_stackedWidget->setCurrentIndex(WID_CONTATOS);
    setVisibilidadeDosElementos(WID_CONTATOS);

    //Setando a ui na tela
    ui->m_vLayout->addWidget(m_stackedWidget);
}

void MainContatos::setVisibilidadeDosElementos(int a_indexWidget)
{
    if (a_indexWidget == WID_CONTATOS) {
        ui->m_pbCancel->show();
        ui->m_pbOk->show();
        ui->m_pbExcluir->hide();
        ui->m_pbEditar->hide();
    } else
    if (a_indexWidget == WID_EDICAO) {
        ui->m_pbCancel->hide();
        ui->m_pbOk->hide();
        ui->m_pbExcluir->show();
        ui->m_pbEditar->show();
    }
}

void MainContatos::style()
{
    QString css = QString();
}

void MainContatos::gerarConnects()
{
    //Colocando connects
    connect(m_listaDeEdicao, SIGNAL(signal_editModel(QAbstractItemModel*)),
            m_widContatos, SLOT(on_criaModel(QStandardItemModel*)));

    //Connects para lista de edição
    connect(m_listaDeEdicao
          , SIGNAL(signal_deleteRow(QString,QString))
          , m_connectionDB
          , SLOT(slot_deleteRow(QString,QString)));

    connect(m_listaDeEdicao
          , SIGNAL(signal_editModel(QString,QString,QString,QString))
          , m_connectionDB
          , SLOT(slot_update(QString,QString,QString,QString)));

    //Preenchimento do model
    connect(m_connectionDB
          , SIGNAL(signal_criarLista())
          , this
          , SLOT(slot_preencheListaDeContatos()));

    connect(m_listaDeEdicao
          , SIGNAL(signal_refreshmodel())
          , this
          , SLOT(slot_preencheListaDeContatos()));
}

void MainContatos::centralizeMyWidget(QWidget *a_miMod, double a_spacer, double a_percentMaxHeigth, double a_percentMaxWidth)
{
    //Setando haltura e largura geométrica máxima para elementos modais
    if (a_percentMaxHeigth > 0
    &&  a_percentMaxWidth > 0
    &&  a_miMod->parentWidget()) {
        a_miMod->setMaximumHeight(a_miMod->parentWidget()->height() * a_percentMaxHeigth);
        a_miMod->setMaximumWidth(a_miMod->parentWidget()->height() * a_percentMaxWidth);
    }

    //Setando as flags modais e definindo o novo tamanho para widget
    a_miMod->setWindowFlags(Qt::Dialog);
    a_miMod->setWindowModality(Qt::ApplicationModal);
    QDesktopWidget* l_desk = QApplication::desktop();
    QSize l_newsize = QSize (l_desk->width()/a_spacer,l_desk->height()/a_spacer);

    a_miMod->setVisible(true);
    a_miMod->raise();
    a_miMod->resize(l_newsize);
    a_miMod->updateGeometry();

    //Centralizando
    QSize l_size = a_miMod->size();
    int w = l_desk->width();
    int h = l_desk->height();
    int mw = l_size.width();
    int mh = l_size.height();
    int cw = (w/2) - (mw/2);
    int ch = (h/2) - (mh/2);

    a_miMod->move(cw,ch);

    QApplication::setActiveWindow(a_miMod);
    a_miMod->setFocus();
}

void MainContatos::iniciarWidListaDeContados()
{
    if (!m_listaDeEdicao) {
        m_listaDeEdicao = new ListParaedicao(this);
    }
}

void MainContatos::iniciarWidContatos()
{
    if (!m_widContatos) {
        m_widContatos = new WidContatos(this);
    }
}

void MainContatos::iniciarWidTradutor()
{
    if (!m_widTradutor) {
        m_widTradutor = new WidTradutor(this);
        m_widTradutor->setWindowTitle(tr("Idiomas"));
        m_widTradutor->criaModelTradutor();
        m_widTradutor->hide();
    }

    #ifndef Q_OS_ANDROID
        m_widTradutor->setMaximumHeight(this->height() * 0.80);
        m_widTradutor->setMinimumHeight(this->height() * 0.60);
        m_widTradutor->setWindowFlags(Qt::Dialog);
        m_widTradutor->setWindowModality(Qt::ApplicationModal);
    #endif
}

void MainContatos::setOrientation()
{
    QDesktopWidget* desk = QApplication::desktop();
    if (desk->height() > desk->width()) {
        m_orientation = Qt::Vertical;
    } else {
        m_orientation = Qt::Horizontal;
    }
}

void MainContatos::resizeEvent(QResizeEvent * a_event)
{
    QSize size = a_event->size();
    bool isLandScape = size.width() > size.height();

    if (isLandScape
    &&  m_orientation != Qt::Horizontal) {
        m_orientation = Qt::Horizontal;

    } else {
        m_orientation = Qt::Vertical;
    }

#ifdef Q_OS_ANDROID
    if (m_widTradutor->isVisible()) {
        centralizeMyWidget(m_widTradutor, 1.1);
    }
#endif

    QWidget::resizeEvent(a_event);
}

void MainContatos::closeEvent(QCloseEvent *a_event)
{
    QWidget::closeEvent(a_event);
}

void MainContatos::slot_preencheListaDeContatos()
{
    // Seto o resultado do select na listview
    m_connectionDB->selectAll(m_model);
    m_widContatos->on_criaModel(m_model);
    m_listaDeEdicao->addModelCadastrados(m_model);
}

void MainContatos::slot_criaModelTradutor()
{
    //Pegar os itens do arquivo de resource e setar na QListView
    m_widTradutor->criaModelTradutor();
}

void MainContatos::on_m_pbNovo_clicked()
{
    m_stackedWidget->setCurrentIndex(WID_CONTATOS);
    setVisibilidadeDosElementos(WID_CONTATOS);
}

void MainContatos::on_m_pbLista_clicked()
{
    m_stackedWidget->setCurrentIndex(WID_EDICAO);
    setVisibilidadeDosElementos(WID_EDICAO);
}

void MainContatos::on_m_pbAbout_clicked()
{
    QMessageBox::information(0
                           , tr("QContatos")
                           , tr("Esse programa é um exemplo para listagem de contatos via SQL")
                           , QMessageBox::Ok);
}

void MainContatos::on_m_pbTradutor_clicked()
{
    #ifdef Q_OS_ANDROID
        centralizeMyWidget(m_widTradutor, 1.1);
    #else
        m_widTradutor->show();
    #endif
}

void MainContatos::on_m_pbCancel_clicked()
{
    int index = m_stackedWidget->currentIndex();

    if (index == WID_CONTATOS) {
        m_widContatos->nome("");
        m_widContatos->contato("");
    } else {
        m_listaDeEdicao->nome("");
        m_listaDeEdicao->contato("");
    }
}

void MainContatos::on_m_pbOk_clicked()
{
    bool ok = false;
    int validacao = REST_OK;
    int index = m_stackedWidget->currentIndex();
    QStringList listValues = QStringList();

    if (index == WID_CONTATOS) {
        validacao = m_widContatos->verificarRestricoes();

        listValues << m_widContatos->nome();
        listValues << m_widContatos->contato();
    } else {
        validacao = m_listaDeEdicao->verificarRestricoes();

        listValues << m_listaDeEdicao->nome();
        listValues << m_listaDeEdicao->contato();
    }

    switch (validacao) {
    case NOME_EMPTY:
        QMessageBox::warning(this
                           , QString(tr("Atenção"))
                           , QString(tr("Campo 'Nome' está vazio"))
                           , QMessageBox::Ok);
        break;

    case CONTATO_EMPTY:
        QMessageBox::warning(this
                           , QString(tr("Atenção"))
                           , QString(tr("Campo 'Contato' está vazio"))
                           , QMessageBox::Ok);
        break;
    default:
        break;
    }

    if (validacao != REST_OK) {
        return;
    }

    ok = m_connectionDB->insertTable("contato_db", listValues);

    if (ok) {
        on_m_pbCancel_clicked();
        slot_preencheListaDeContatos();
    }
}

void MainContatos::on_m_pbExcluir_clicked()
{
    m_listaDeEdicao->on_m_pbExcluir_clicked();
}

void MainContatos::on_m_pbEditar_clicked()
{
    m_listaDeEdicao->on_m_pbEditar_clicked();
}
