#include "widtradutor.h"
#include "ui_widtradutor.h"

#include <QStandardItemModel>

WidTradutor::WidTradutor(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::WidTradutor)
{
    ui->setupUi(this);
}

WidTradutor::~WidTradutor()
{
    delete ui;
}

void WidTradutor::criaModelTradutor()
{
    QStandardItemModel * model = new QStandardItemModel(this);
    QStandardItem * itemModel_1;
    QStandardItem * itemModel_2;
    QStandardItem * itemModel_3;

    itemModel_1 = new QStandardItem(QIcon(":/imagens/br.png"), tr("Brasil"));
    itemModel_2 = new QStandardItem(QIcon(":/imagens/um.png"), tr("Inglês"));
    itemModel_3 = new QStandardItem(QIcon(":/imagens/es.png"), tr("Espanhol"));

    model->appendRow(itemModel_1);
    model->appendRow(itemModel_2);
    model->appendRow(itemModel_3);

    ui->m_listIdioma->setModel(model);
}
