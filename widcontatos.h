#ifndef WIDCONTATOS_H
#define WIDCONTATOS_H

#include <QWidget>
#include <QStandardItemModel>

enum {
    NOME_EMPTY_CONTATO = 1,
    CONTATO_EMPTY_CONTATO = 2,
    REST_OK_CONTATO = 0
};

namespace Ui {
class WidContatos;
}

class WidContatos : public QWidget
{
    Q_OBJECT

public:
    explicit WidContatos(QWidget *parent = 0);
    ~WidContatos();

    int verificarRestricoes();
    QString nome();
    QString contato();
    void nome(QString a_text);
    void contato(QString a_text);

public slots:
    void on_criaModel(QStandardItemModel * a_model);

private:
    Ui::WidContatos *ui;
};

#endif // WIDCONTATOS_H
