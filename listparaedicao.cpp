#include "listparaedicao.h"
#include "ui_listparaedicao.h"

ListParaedicao::ListParaedicao(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::ListParaedicao)
{
    ui->setupUi(this);
}

ListParaedicao::~ListParaedicao()
{
    delete ui;
}

void ListParaedicao::addModelCadastrados(QStandardItemModel *a_model)
{
    ui->listView->setModel(a_model);
}

int ListParaedicao::verificarRestricoes()
{
    if (ui->m_leNome->text().isEmpty())
    {
        return NOME_EMPTY;
    }

    if (ui->m_leContatos->text().isEmpty())
    {
        return CONTATO_EMPTY;
    }

    return REST_OK;
}

QString ListParaedicao::nome()
{
    return ui->m_leNome->text();
}

QString ListParaedicao::contato()
{
    return ui->m_leContatos->text();
}

void ListParaedicao::nome(QString a_text)
{
    ui->m_leNome->setText(a_text);
}

void ListParaedicao::contato(QString a_text)
{
    ui->m_leContatos->setText(a_text);
}

void ListParaedicao::on_listView_clicked(const QModelIndex &index)
{
    //Capturando a string da seleção na QListView
    QString itemText = index.data(Qt::DisplayRole).toString();

    //Adicionando as QStrings para os QLineEdits
    ui->m_leNome->setText(itemText.split(" - ").value(0));
    ui->m_leContatos->setText(itemText.split(" - ").value(1));
}

void ListParaedicao::on_m_pbEditar_clicked()
{
    if (!ui->m_leNome->text().isEmpty()
    ||  !ui->m_leContatos->text().isEmpty()) {

        //Pego o index da lista
        QModelIndex index = ui->listView->currentIndex();

        //Pego os valores antigos
        QString antigoValor = index.data(Qt::DisplayRole).toString();

        //Faço a Edição da linha
        QString lineEdit = ui->m_leNome->text() + " - " + ui->m_leContatos->text();

        //Passo o QModelIndex e o texto da linha que editei para a minha QListView
        ui->listView->model()->setData(index, QVariant(lineEdit));

        //Limpo os campos
        ui->m_leNome->setText("");
        ui->m_leContatos->setText("");

//        emit signal_editModel((QStandardItemModel*)ui->listView->model());

        QString novoValor = index.data(Qt::DisplayRole).toString();

        //Emitindo signal para deleção da row na list view
        emit signal_editModel(novoValor.split(" - ").value(0)
                            , novoValor.split(" - ").value(1)
                            , antigoValor.split(" - ").value(0)
                            , antigoValor.split(" - ").value(1));
    }
}

void ListParaedicao::on_m_pbExcluir_clicked()
{
    //Capturando a string da seleção na QListView
    QString itemText = ui->listView->currentIndex().data(Qt::DisplayRole).toString();

    //Emitindo signal para deleção da row na list view
    emit signal_deleteRow(itemText.split(" - ").value(0)
                        , itemText.split(" - ").value(1));

    emit signal_refreshmodel();

    //Removendo do model, exemplo simples remoção
//    QModelIndex modelIndex = ui->listView->currentIndex();
//    ui->listView->model()->removeRow(modelIndex.row());
}
