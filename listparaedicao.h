#ifndef LISTPARAEDICAO_H
#define LISTPARAEDICAO_H

#include <QWidget>
#include <QStandardItemModel>

enum {
    NOME_EMPTY = 1,
    CONTATO_EMPTY = 2,
    REST_OK = 0
};

namespace Ui {
class ListParaedicao;
}

class ListParaedicao : public QWidget
{
    Q_OBJECT

public:
    explicit ListParaedicao(QWidget *parent = 0);
    ~ListParaedicao();

    void addModelCadastrados(QStandardItemModel * a_model);
    int verificarRestricoes();
    QString nome();
    QString contato();
    void nome(QString a_text);
    void contato(QString a_text);

signals:
    void signal_deleteRow(QString a_nome, QString a_contato);
    void signal_editModel(QString a_nomeNovo, QString a_contatoNovo, QString a_nomeAnterior, QString a_contatoAnterior);
    void signal_refreshmodel();

public slots:
    void on_listView_clicked(const QModelIndex &index);

    void on_m_pbEditar_clicked();

    void on_m_pbExcluir_clicked();

private:
    Ui::ListParaedicao *ui;
};

#endif // LISTPARAEDICAO_H
