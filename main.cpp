#include "maincontatos.h"
#include "connection.h"
#include <QApplication>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);

    //Carrego o banco de dados
    connection::instanceDB();

    MainContatos w;
    w.show();

    return a.exec();
}
