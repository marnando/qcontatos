#include "widcontatos.h"
#include "ui_widcontatos.h"

WidContatos::WidContatos(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::WidContatos)
{
    ui->setupUi(this);
}

WidContatos::~WidContatos()
{
    delete ui;
}

int WidContatos::verificarRestricoes()
{
    if (ui->m_leNome->text().isEmpty())
    {
        return NOME_EMPTY_CONTATO;
    }

    if (ui->m_leContatos->text().isEmpty())
    {
        return CONTATO_EMPTY_CONTATO;
    }

    return REST_OK_CONTATO;
}

QString WidContatos::nome()
{
    return ui->m_leNome->text();
}

QString WidContatos::contato()
{
    return ui->m_leContatos->text();
}

void WidContatos::nome(QString a_text)
{
    ui->m_leNome->setText(a_text);
}

void WidContatos::contato(QString a_text)
{
    ui->m_leContatos->setText(a_text);
}

void WidContatos::on_criaModel(QStandardItemModel * a_model)
{
    ui->m_lvContatos->setModel(a_model);
}
