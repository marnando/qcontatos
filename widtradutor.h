#ifndef WIDTRADUTOR_H
#define WIDTRADUTOR_H

#include <QWidget>

namespace Ui {
class WidTradutor;
}

class WidTradutor : public QWidget
{
    Q_OBJECT

public:
    explicit WidTradutor(QWidget *parent = 0);
    ~WidTradutor();

    void criaModelTradutor();

private:
    Ui::WidTradutor *ui;
};

#endif // WIDTRADUTOR_H
