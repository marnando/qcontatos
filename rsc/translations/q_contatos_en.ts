<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="en_US">
<context>
    <name>ListParaedicao</name>
    <message>
        <location filename="../../listparaedicao.ui" line="14"/>
        <source>Form</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../listparaedicao.ui" line="32"/>
        <source>Cadastrados</source>
        <translation>Registered</translation>
    </message>
    <message>
        <location filename="../../listparaedicao.ui" line="41"/>
        <source>Selcione um item na lista</source>
        <translation>Selcione an item in the list</translation>
    </message>
    <message>
        <location filename="../../listparaedicao.ui" line="53"/>
        <source>Nome</source>
        <translation>Name</translation>
    </message>
    <message>
        <location filename="../../listparaedicao.ui" line="60"/>
        <source>Contato</source>
        <translation>Contact</translation>
    </message>
    <message>
        <location filename="../../listparaedicao.ui" line="87"/>
        <source>Excluir</source>
        <translation>Delete</translation>
    </message>
    <message>
        <location filename="../../listparaedicao.ui" line="94"/>
        <source>Editar</source>
        <translation>Edit</translation>
    </message>
</context>
<context>
    <name>MainContatos</name>
    <message>
        <location filename="../../maincontatos.ui" line="14"/>
        <source>MainContatos</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../maincontatos.ui" line="35"/>
        <source>Novo</source>
        <translation>New</translation>
    </message>
    <message>
        <location filename="../../maincontatos.ui" line="54"/>
        <source>Lista</source>
        <translation>List</translation>
    </message>
    <message>
        <location filename="../../maincontatos.ui" line="73"/>
        <source>Sobre</source>
        <translation>About</translation>
    </message>
    <message>
        <location filename="../../maincontatos.ui" line="93"/>
        <source>Casdastro</source>
        <translation>Cadastre</translation>
    </message>
    <message>
        <location filename="../../maincontatos.ui" line="101"/>
        <source>Nome:</source>
        <translation>Name:</translation>
    </message>
    <message>
        <location filename="../../maincontatos.ui" line="108"/>
        <source>Contato:</source>
        <translation>Contact:</translation>
    </message>
    <message>
        <location filename="../../maincontatos.ui" line="136"/>
        <source>Cadastrados</source>
        <translation>Registred</translation>
    </message>
    <message>
        <location filename="../../maincontatos.ui" line="194"/>
        <source>Cancelar</source>
        <translation>Cancel</translation>
    </message>
    <message>
        <location filename="../../maincontatos.ui" line="207"/>
        <source>Ok</source>
        <translation>Ok</translation>
    </message>
    <message>
        <location filename="../../maincontatos.cpp" line="18"/>
        <location filename="../../maincontatos.cpp" line="149"/>
        <source>QContatos</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../maincontatos.cpp" line="33"/>
        <source>Idiomas</source>
        <translation>Languages</translation>
    </message>
    <message>
        <location filename="../../maincontatos.cpp" line="94"/>
        <location filename="../../maincontatos.cpp" line="101"/>
        <source>Atenção</source>
        <translation>Attention</translation>
    </message>
    <message>
        <location filename="../../maincontatos.cpp" line="95"/>
        <source>Campo &apos;Nome&apos; está vazio</source>
        <translation>Field &apos;Name&apos; is empty</translation>
    </message>
    <message>
        <location filename="../../maincontatos.cpp" line="102"/>
        <source>Campo &apos;Contato&apos; está vazio</source>
        <translation>Field &apos;Contact&apos; is empty</translation>
    </message>
    <message>
        <location filename="../../maincontatos.cpp" line="150"/>
        <source>Esse programa é um exemplo para listagem de contatos via SQL</source>
        <translation>This is an exeample program to show a list of contats by SQL</translation>
    </message>
</context>
<context>
    <name>WidTradutor</name>
    <message>
        <location filename="../../widtradutor.ui" line="14"/>
        <source>Form</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../widtradutor.ui" line="20"/>
        <source>Selecione um Idioma</source>
        <translation>Select a language</translation>
    </message>
    <message>
        <location filename="../../widtradutor.cpp" line="25"/>
        <source>Brasil</source>
        <translation>Brazil</translation>
    </message>
    <message>
        <location filename="../../widtradutor.cpp" line="26"/>
        <source>Inglês</source>
        <translation>English</translation>
    </message>
    <message>
        <location filename="../../widtradutor.cpp" line="27"/>
        <source>Espanhol</source>
        <translation>Spanish</translation>
    </message>
</context>
<context>
    <name>connection</name>
    <message>
        <location filename="../../connection.cpp" line="124"/>
        <source>Não pode abrir um Database</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../connection.cpp" line="125"/>
        <source>Não há uma conexão de estabilidade para um database.
Esse exemplo precisa de suporte ao SQLite. Por favor leia a documentation Qt SQL driver para informações de como executá-lo.

Clique em cancelar para finalizar.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
</TS>
