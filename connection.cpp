#include "connection.h"

#include <QObject>
#include <QMessageBox>
#include <QSqlError>
#include <QSqlQuery>
#include <QDebug>

QSqlDatabase * connection::m_db = NULL;
connection::connection(QObject *parent) :
    QObject(parent)
{
}

connection::~connection()
{
    connection::release();
}

bool connection::createTable(QString a_tableName, QList<QPair<QString, QString> > a_listCamposTipo)
{
    QString sql;
    QString endSQL;
    QString token;
    QSqlQuery query(*m_db);
    QPair<QString, QString> values;
    bool ok = false;

    ok = query.exec(QString(" DROP TABLE %1; ").arg(a_tableName));

    sql = QString(" CREATE TABLE %1 ( ").arg(a_tableName);
    token = QString(" , ");
    endSQL = QString( " );" );

    int count = 1;
    if (!a_listCamposTipo.isEmpty()) {
        foreach (values, a_listCamposTipo) {
            sql = sql + values.first + " " + values.second;

            if (count < a_listCamposTipo.size()) {
                sql = sql + token;
            }

            count++;
        }
    }

    sql = sql + endSQL;

    ok = query.exec(sql);

    return ok;
}

bool connection::insertTable(QString a_tableName, QStringList a_listValues)
{
    QString sql;
    QString endSQL;
    QString token;
    QSqlQuery query(*m_db);
    QString value;
    bool ok;

    sql = QString(" INSERT OR REPLACE INTO %1 VALUES ( ").arg(a_tableName);
    token = QString(" , ");
    endSQL = QString( " );" );

    int count = 1;
    if (!a_listValues.isEmpty()) {
        foreach (value, a_listValues) {
            sql = sql + QString("'%1'").arg(value);

            if (count < a_listValues.size()) {
                sql = sql + token;
            }

            count++;
        }
    }

    sql = sql + endSQL;

    ok = query.exec(sql);

    return ok;
}

bool connection::deleteRow(QString a_tableName)
{
    Q_UNUSED(a_tableName)
    return true;
}

bool connection::slot_update(QString a_nomeNovo, QString a_contatoNovo, QString a_nomeAntigo, QString a_contatoAntigo)
{
    QSqlQuery query(*m_db);

    QString sql = QString(" UPDATE contato_db "
                          "    SET nome = '%1' "
                          "    AND contato = '%2' "
                          "  WHERE nome = '%3' "
                          "    AND contato = '%4' ")
                         .arg(a_nomeNovo)
                         .arg(a_contatoNovo)
                         .arg(a_nomeAntigo)
                         .arg(a_contatoAntigo);

    return query.exec(sql);
}

QStandardItemModel * connection::selectAll(QStandardItemModel *a_model)
{
    QSqlQuery query(*m_db);

    if (query.exec(" SELECT * FROM contato_db; ")) {

        // Limpando o model para receber novos valores
        a_model->clear();

         while(query.next()) {
            // Adicionando linhas no model
            a_model->appendRow(new QStandardItem(query.value(0).toString()
                                               + " - "
                                               + query.value(1).toString()));
         }
    }
    return a_model;
}

bool connection::createConnection()
{
    // Conecto o database
    m_db = new QSqlDatabase(QSqlDatabase::addDatabase("QSQLITE"));

    // Seto um nome para omeu database
    m_db->setDatabaseName("teste");

    // Verifico se consigo carregá-lo
    if (!m_db->open()) {

        QMessageBox::critical(0
                            , tr("Não pode abrir um Database")
                            , tr("Não há uma conexão de estabilidade para um database.\n"
                                 "Esse exemplo precisa de suporte ao SQLite. Por favor leia "
                                 "a documentation Qt SQL driver para informações de como "
                                 "executá-lo.\n\n"
                                 "Clique em cancelar para finalizar.")
                            , QMessageBox::Cancel);

        return false;
    }

    return true;
}

QSqlDatabase *connection::instanceDB()
{
    if (!m_db)
        createConnection();

    return m_db;
}

void connection::release()
{
    if (m_db){
        m_db->close();
        delete m_db;
        m_db = NULL;
    }
}

void connection::slot_deleteRow(QString a_nome, QString a_contato)
{
    QString sql = QString();
    QSqlQuery query(*m_db);

    sql = QString(" DELETE "
                  "   FROM contato_db "
                  "  WHERE nome = '%1' "
                  "    AND contato = '%2' ; ")
                 .arg(a_nome).arg(a_contato);
    query.exec(sql);

    emit signal_criarLista();
}
