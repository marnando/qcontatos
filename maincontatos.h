#ifndef MAINCONTATOS_H
#define MAINCONTATOS_H

#include <QMainWindow>
#include <QStandardItemModel>
#include <QStackedWidget>
#include <QResizeEvent>
#include <QCloseEvent>

enum {
    WID_CONTATOS    = 0,
    WID_EDICAO      = 1
};

namespace Ui {
class MainContatos;
}

class connection;
class ListParaedicao;
class WidTradutor;
class WidContatos;
class MainContatos : public QMainWindow
{
    Q_OBJECT

public:
    QStandardItemModel * m_model;

private:
    connection      * m_connectionDB;
    ListParaedicao  * m_listaDeEdicao;
    WidTradutor     * m_widTradutor;
    WidContatos     * m_widContatos;
    QStackedWidget  * m_stackedWidget;

    Qt::Orientation   m_orientation;

public:
    explicit MainContatos(QWidget *parent = 0);
    ~MainContatos();

private:
    void setSizeModalWidget(QWidget * a_widget, int a_parentHeight, int a_parentWidth);
    void initUi();
    void setVisibilidadeDosElementos(int a_indexWidget);
    void style();
    void gerarConnects();
    void centralizeMyWidget(QWidget *a_miMod
                          , double a_spacer = 1.5
                          , double a_percentMaxHeigth = 0
                          , double a_percentMaxWidth = 0);
    void iniciarWidListaDeContados();
    void iniciarWidContatos();
    void iniciarWidTradutor();
    void setOrientation();

protected:
    void resizeEvent(QResizeEvent * a_event);
    void closeEvent(QCloseEvent * a_event);

signals:

private slots:
    void slot_preencheListaDeContatos();
    void slot_criaModelTradutor();
    void on_m_pbNovo_clicked();
    void on_m_pbLista_clicked();
    void on_m_pbAbout_clicked();
    void on_m_pbTradutor_clicked();
    void on_m_pbCancel_clicked();
    void on_m_pbOk_clicked();
    void on_m_pbExcluir_clicked();
    void on_m_pbEditar_clicked();

private:
    Ui::MainContatos *ui;
};

#endif // MAINCONTATOS_H
